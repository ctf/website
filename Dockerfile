FROM nginx:alpine

ADD index.html /var/www/html/
ADD /assets /var/www/html/assets/
ADD /images /var/www/html/images/
ADD nginx.conf /etc/nginx/conf.d/default.conf

CMD ["nginx", "-g", "daemon off;"]
